﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Net;

public class DataClient : MonoBehaviour {

	public string MentorHostname = "128.10.9.56";
	public int MentorDataPort = 55056;
	public bool AlwaysReconnect = true;
	public float ReconnectSeconds = 5.0f;


	private TcpClient client;

	const int READ_BUFFER_SIZE = 1048576;
	private byte[] readBuffer = new byte[READ_BUFFER_SIZE];

	private CommandInterpreter commandInterpreter;

	private bool shuttingDown = false;

	private bool requestingReset = false;

	// Use this for initialization
	void Start () {
		commandInterpreter = CommandInterpreter.Instance;	// need to get a reference to this here rather than in the async callback

		StartCoroutine (ConnectToMentor (0));

		//string mockCommand = "{ 'command': 'SendAnnotationsCommand' }";
		//CommandInterpreter.Instance.ProcessCommand (mockCommand);
	}

	IEnumerator ConnectToMentor(float delaySeconds) {
		Debug.Log ("ConnectToMentor, delaying for " + delaySeconds + " sec");

		yield return new WaitForSeconds (delaySeconds);
		Debug.Log ("done waiting");

		try {
			// to force Unity to actually use a particular client data port:
			//IPEndPoint ipEndPoint = new IPEndPoint (IPAddress.Any, TraineeDataPort); 
			//client = new TcpClient (ipEndPoint);
			//client.Client.Connect (MentorHostname, MentorDataPort);

			// to allow the local system to decide its own local port
			client = new TcpClient(MentorHostname, MentorDataPort);

			// start async read
			client.GetStream().BeginRead(readBuffer, 0, READ_BUFFER_SIZE, new AsyncCallback(DoRead), null);
		} catch (Exception e) {
			Debug.LogError ("got exception: " + e.Message);
			Debug.LogError (e.StackTrace);
			ResetSocket ();
		}
	}

	private void DoRead(IAsyncResult ar) {
		try {
			Debug.Log ("DoRead");
			int BytesRead;

			// finish async read into readbuffer and return the number of bytes read.
			BytesRead = client.GetStream ().EndRead (ar);

			Debug.Log ("BytesRead: " + BytesRead);

			if (BytesRead < 1) {
			 	//no bytes read, the server has closed
				if (!shuttingDown) {
					ResetSocket();
				}
				return;
			}

			string incomingCommandJsonString = Encoding.ASCII.GetString (readBuffer, 0, BytesRead);
			Debug.Log ("incomingCommandJsonString: " + incomingCommandJsonString);

			if (BytesRead > 0) {
				try {
					commandInterpreter.ProcessCommand (incomingCommandJsonString);
				} catch (Exception e) {
					Debug.LogError("exception when attempting to process command, throwing away command");
					Debug.LogError(e.Message);
					Debug.LogError(e.StackTrace);
				}

			}

			// start a new async read into readBuffer
			client.GetStream().BeginRead(readBuffer, 0, READ_BUFFER_SIZE, new AsyncCallback(DoRead), null);
		} catch (Exception e) {
			Debug.LogError ("got exception: " + e.Message);
			Debug.LogError (e.StackTrace);
			if (!shuttingDown) {
				ResetSocket();
			}
		}

	}

	void ResetSocket() {
		Debug.Log ("ResetSocket");
		CleanupSocket ();

		if (AlwaysReconnect) {
			requestingReset = true;
		}
	}

	void CleanupSocket() {
		if (client != null) {
			Debug.Log ("closing client");
			if (client.Connected) {
				//client.GetStream ().Close ();
				client.Close ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (requestingReset) {
			requestingReset = false;
			StartCoroutine (ConnectToMentor (ReconnectSeconds));
		}
	}

	void OnDestroy() {
		shuttingDown = true;
		CleanupSocket ();
	}
}
