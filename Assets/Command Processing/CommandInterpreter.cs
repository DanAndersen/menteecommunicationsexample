using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;

// This script is responsible for interpreting an incoming command JSON from the mentor system. 
// After the mentee local client has received a string line from the mentor, it hands it to the CommandInterpreter.
// The CommandInterpreter parses it as JSON and then, depending on the structure of the command, 
// will trigger the creation/update/deletion of annotations.

// Uses the SimpleJSON library: http://wiki.unity3d.com/index.php/SimpleJSON

public class CommandInterpreter : Singleton<CommandInterpreter> {
	protected CommandInterpreter () {}

	enum CommandType {SendAnnotations, CreateAnnotation, DeleteAnnotation, UpdateAnnotation, INVALID};

	public void ProcessCommand(string incomingCommandJsonString) {
		Debug.Log ("Processing command: " + incomingCommandJsonString);

		JSONNode root = JSON.Parse (incomingCommandJsonString);
		Debug.Log ("root == null? " + (root == null));

		CommandType commandType = GetCommandType (root);

		if (commandType == CommandType.INVALID) {
			Debug.LogError ("missing command type");
			return;
		}

		if (commandType == CommandType.SendAnnotations) {

			JSONNode annotationState = root ["annotation_state"];

			throw new NotImplementedException ("here we need to clear all existing annotations and replace them with the annotations in the provided annotation state");
		} else if (commandType == CommandType.CreateAnnotation) {

			JSONNode annotationMemoryToCreate = root ["annotation_memory"];

			throw new NotImplementedException ("here we need to create the new annotation based on this annotation memory data");
		} else if (commandType == CommandType.DeleteAnnotation) {
			
			int annotationMemoryIdToDelete = root ["id"].AsInt;

			throw new NotImplementedException ("here we need to actually delete the annotation");
		} else if (commandType == CommandType.UpdateAnnotation) {

			int annotationMemoryIdToUpdate = root ["id"].AsInt;
			JSONNode updatedAnnotationMemory = root ["annotation_memory"];

			throw new NotImplementedException ("here we need to actually update the annotation based on the new data");
		} else {
			Debug.Log ("unimplemented command type: " + commandType);
		}


	}

	private CommandType GetCommandType(JSONNode root) {
		if (root ["command"] == null) {
			return CommandType.INVALID;
		}

		JSONNode commandType = root ["command"];

		if (commandType.Value.Equals ("SendAnnotationsCommand")) {
			return CommandType.SendAnnotations;
		} else if (commandType.Value.Equals ("CreateAnnotationCommand")) {
			return CommandType.CreateAnnotation;
		} else if (commandType.Value.Equals ("DeleteAnnotationCommand")) {
			return CommandType.DeleteAnnotation;
		} else if (commandType.Value.Equals ("UpdateAnnotationCommand")) {
			return CommandType.UpdateAnnotation;
		} else {
			return CommandType.INVALID;
		}
	}
}
